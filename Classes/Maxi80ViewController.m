//
//  Maxi80ViewController.m
//  Maxi80
//
//  Created by Sébastien Stormacq on 07/10/09.
//  Copyright 2009 Sébastien Stormacq. All rights reserved.
//
//
// This file is part of Maxi80 iPhone Application.
// 
// Maxi80 iPhone App is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2 of the License AND the 
// Common Development and Distribution License. 
// 
// Maxi80 iPhone App is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the Licenses
// along with Maxi80 iPhone App.  If not, see <http://www.gnu.org/licenses/>
// and http://www.sun.com/cddl/cddl.html.
//
//


#import "Maxi80ViewController.h"
#import <dispatch/dispatch.h>
#import "StatisticsCollector.h"

//for iTMS URL loading and parsing
#import "JSON.h"
//#import "GTMRegex.h"

#define MAXI80_APPS_SETTINGS @"http://maxi80.com/iphone/data_binary.plist"
#define MAIL_SUBJECT @"Message via Maxi80 iPhone"

@implementation Show

@synthesize name;
@synthesize hostName;
@synthesize hostEmail;
@synthesize url;
@synthesize startHour;
@synthesize endHour;

@end

@implementation Maxi80ViewController

@synthesize iAdsView;
@synthesize lbHelp;
@synthesize toolbar;
@synthesize btPlay;
@synthesize btPause;
@synthesize btVolume;
@synthesize btDonate;
@synthesize btSend;
@synthesize lbSongName;
@synthesize scrollView;

//show name
@synthesize viewShowName, viewShowAndCover, ivEnveloppe, ivLogo;
@synthesize lbCurrentShowName, lbCurrentShowHost, lbCurrentShowTime;
@synthesize lbNextShowName, lbNextShowTime, lbSendMail;

//link to iTMS
@synthesize ivITMSButton, ivITMSArtWork, itmsURL, itmsGroupView;

@synthesize settings;
@synthesize currentShow;
@synthesize volumeSlider;


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

#pragma mark Mail handling

-(void)sendMailTo:(NSString*)to withSubject:(NSString*)subject andMessage:(NSString*)msg {
	
	MFMailComposeViewController* mailer = [[MFMailComposeViewController alloc] init];
	[mailer setSubject:subject];
	if (to)
		[mailer setToRecipients:[NSArray arrayWithObject:to]];
	else {
		
		//this avoids a rare crash when user tries to send an email when preferences are not loaded yet
		
		NSString* radioEmail = [settings objectForKey:@"radioEmail"];
		
		if (radioEmail)
			[mailer setToRecipients:[NSArray arrayWithObject:radioEmail]];
		else
			[mailer setToRecipients:[NSArray arrayWithObject:@"seb@maxi80.com"]];
	}
	
	mailer.mailComposeDelegate = self;
	[mailer setMessageBody:msg isHTML:NO];
    
    [self resignFirstResponder]; //to allow keyboard event to flow to the mail composer
	[self presentModalViewController:mailer animated:YES];
	
}

-(void)sendMailTo:(NSString*)to withSubject:(NSString*)subject {
	[self sendMailTo:to withSubject:subject andMessage:@""];
}

-(IBAction)sendMessage:(id)sender {
	
	UIActionSheet* actionSheet;
	
	if (![ivCover isCoverAvailable] && playing) {
		NSLog(@"Cover not available - showing long menu");
		actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"207", @"Envoyer un message") delegate:self 
										 cancelButtonTitle:NSLocalizedString(@"208", @"Annuler") 
									destructiveButtonTitle:nil 
										 otherButtonTitles:NSLocalizedString(@"209", @"A l'animateur"), 
					   NSLocalizedString(@"210", @"A la radio"), 
					   NSLocalizedString(@"211", @"Indiquer une pochette manquante"), nil];
	} else {
		NSLog(@"Cover Available - showing short menu");
		actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"207", @"Envoyer un message") delegate:self 
										 cancelButtonTitle:NSLocalizedString(@"208", @"Annuler") 
									destructiveButtonTitle:nil 
										 otherButtonTitles:NSLocalizedString(@"209", @"A l'animateur"), 
					   NSLocalizedString(@"210", @"A la radio"), nil];
	}	
	
	[actionSheet showFromToolbar:self.toolbar];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	
	[self dismissModalViewControllerAnimated:YES];
	[controller release];
    [self becomeFirstResponder]; //to receive remote control notification again

	
	//resume songname animation (in some rare case when the app is in background with the Mail view open, the animation did not restart)
	[lbSongName resumeAnimation];
	
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	NSLog(@"ActionSheet, button clicked : %d", buttonIndex);
	switch (buttonIndex) {
		case 0:
			[self sendMailTo:currentShow.hostEmail withSubject:MAIL_SUBJECT];
			break;
		case 1:
			[self sendMailTo:[settings objectForKey:@"radioEmail"] withSubject:MAIL_SUBJECT];
			break;
		case 2:
			if (![ivCover isCoverAvailable] && playing) {
				[self sendMailTo:[settings objectForKey:@"missingCoverEmail"] 
					 withSubject:NSLocalizedString(@"212", @"Il manque une pochette") 
					  andMessage:[NSString stringWithFormat:NSLocalizedString(@"213", @"manque pochette texte"), lbSongName.text]];
			}
			break;
		default:
			break;
	}
	[actionSheet release];
}


#pragma mark Error Reporting


-(void)showAlertWithError:(NSError*)error {
	
	NSString* title = NSLocalizedString(@"201", @"Impossible de se connecter");
	NSString* msgBase = NSLocalizedString(@"202", @"Nous ne pouvons pas nous connecter à la radio pour le moment. Vérifiez votre connection Internet et assurez-vous d'être connecté à un réseau 3G ou Wifi.");
	NSString* msg;
	if (error)
		msg = [NSString stringWithFormat:NSLocalizedString(@"203", @"%@\nError Code = %d"), msgBase, [error code]];
	else
		msg = msgBase;
	
	alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"204", @"OK") otherButtonTitles:nil];
	[alert show];			
}


#pragma mark Show Name handling

-(void) hideShowName {
    
    
	NSLog(@"hideShowName");
	[UIView animateWithDuration:0.5f delay:0.0f 
					options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
				    animations:^{
						 //real animation block
						 //set alpha to 0 to make the text disappear
						[viewShowName setAlpha:0.0f];
					}
					 completion:^(BOOL finished) {
					 }
	 ];
	
}

//TODO add animation ?
-(void) displayShow:(Show*)current andNext:(Show*)next {

	NSLog(@"displayShow : %@", current.name);
	lbCurrentShowName.text = current.name;
	lbCurrentShowHost.text = current.hostName;
	lbCurrentShowTime.text = [NSString stringWithFormat:@"%02dh00 - %02dh00", current.startHour, current.endHour];
	
	if (next) {
		lbNextShowName.text = next.name;
		lbNextShowTime.text = [NSString stringWithFormat:@"%02dh00 - %02dh00",next.startHour,next.endHour];
	}

	[UIView animateWithDuration:0.5f delay:0.0f 
						options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
					 animations:^{
						 //set alpha to 1.0 to make the text appear
						 [viewShowName setAlpha:1.0f];
					 }
					 completion:^(BOOL finished) {
					 }
	 ];
	
}

-(void)updateShowName {
	
	//NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    dispatch_queue_t main_queue = dispatch_get_main_queue();
	
	if (!settings) {
		NSLog(@"Can not display Show name because settings is null !");
		return;
	}
	
	NSDictionary* shows = [settings objectForKey:@"Shows"];
	NSArray* programs = [settings objectForKey:@"Programs"];
	
	//current time
	NSDate* now = [NSDate date];
	NSCalendar* calendar = [NSCalendar currentCalendar];
	
	unsigned unitFlags = NSHourCalendarUnit | NSWeekdayCalendarUnit ;
	NSDateComponents *comps = [calendar components:unitFlags fromDate:now];	
	
	//0 is monday for us (NSCalendar uses 1 for Sunday)
	NSInteger weekDay = [comps weekday]  - 2;  // -2 because we are 0 based while NSCalendar is 1 based
	if (weekDay < 0)
		weekDay = 6; //Sunday = 6
	
	NSTimeZone* systemTimeZone = [NSTimeZone systemTimeZone];
	NSInteger systemOffset = [systemTimeZone secondsFromGMT] / 3600;
	NSTimeZone* maxi80TimeZone = [NSTimeZone timeZoneWithName:@"Europe/Paris"];
	NSInteger maxi80Offset = [maxi80TimeZone secondsFromGMT] / 3600;
	
	NSInteger maxi80Hour = [comps hour] - systemOffset + maxi80Offset; 
    NSLog(@"current hour = %d, system offset = %d, maxi80 offset = %d", [comps hour], systemOffset, maxi80Offset);
	if (maxi80Hour<0) {
        NSLog(@"Maxi80Hout < 0, adjusting to previous day");
		maxi80Hour += 24;
		weekDay -= 1;
	}
    if (maxi80Hour>24) {
        NSLog(@"Maxi80Hout > 24, adjusting to next day");
		maxi80Hour -= 24;
		weekDay += 1;
    }
	
	NSLog(@"Hour = %d, weekday = %d", maxi80Hour, weekDay);
	
	//take the slot for today
	NSArray* slots = [programs objectAtIndex:weekDay];
	
	//search the program name
	BOOL found = NO;
	NSInteger j = 0;
	NSInteger originalStopHour = 0; //used to keep track of end time, indepedently of timezone
	while (!found && j < [slots count]) {
		
		NSDictionary* slot = [slots objectAtIndex:j];
		
		NSInteger startHour = [((NSNumber*)[slot objectForKey:@"StartTime"]) intValue];
		NSInteger stopHour = [((NSNumber*)[slot objectForKey:@"EndTime"]) intValue];
		if (maxi80Hour >= startHour && maxi80Hour < stopHour) {
			NSDictionary* show = [shows objectForKey:[slot objectForKey:@"Show"]];
			
			Show* newShow = [[Show alloc] init];
			newShow.name = [show objectForKey:@"Name"];
			newShow.hostName = [show objectForKey:@"Host"];
			newShow.hostEmail = [show objectForKey:@"EMail"];
			newShow.url = [show objectForKey:@"URL"];
			
			//modified to display local time instead of maxi80 time
			//newShow.startHour = startHour;
			//newShow.endHour = stopHour;
			newShow.startHour = startHour - maxi80Offset + systemOffset;
            if (newShow.startHour < 0)
                newShow.startHour += 24;
            
			newShow.endHour = stopHour - maxi80Offset + systemOffset;
            if (newShow.endHour < 0)
                newShow.endHour += 24;
			originalStopHour = stopHour;
			//NSLog(@"Maxi80 START = %d", startHour);
			//NSLog(@"International START = %d", newShow.startHour);
			//NSLog(@"Maxi80 STOP = %d", stopHour);
			//NSLog(@"International STOP = %d", newShow.endHour);
			
			self.currentShow = newShow;
			[newShow release];
			
			found = YES;
		}
		j++;
	}
	
	
	//there is bug here for the last show of the day. 23h00 - 24h00
	//there is no next
	if (found && originalStopHour == 24) {
		weekDay += 1;
		if (weekDay > 6) weekDay = 0;
		originalStopHour = 00;
	}
	
	//take the slot for today (or tomorrow see above)
	slots = [programs objectAtIndex:weekDay];

	//search next program name
	BOOL nextFound = NO;
	NSInteger k = 0;
	Show* nextShow = nil;
	while (!nextFound && k < [slots count]) {
		
		NSDictionary* slot = [slots objectAtIndex:k];
		NSInteger startHour = [((NSNumber*)[slot objectForKey:@"StartTime"]) intValue];
		NSInteger stopHour = [((NSNumber*)[slot objectForKey:@"EndTime"]) intValue];
		if (startHour == originalStopHour) {
			NSDictionary* show = [shows objectForKey:[slot objectForKey:@"Show"]];
			
			nextShow = [[Show alloc] init];
			nextShow.name = [show objectForKey:@"Name"];
			nextShow.hostName = [show objectForKey:@"Host"];
			nextShow.hostEmail = [show objectForKey:@"EMail"];
			nextShow.url = [show objectForKey:@"URL"];
			nextShow.startHour = startHour - maxi80Offset + systemOffset;
			
			if (nextShow.startHour < 0)
				nextShow.startHour = 24 + nextShow.startHour;
			
			nextShow.endHour = stopHour - maxi80Offset + systemOffset;
			
			if (nextShow.endHour < 0 )
				nextShow.endHour = 24 + nextShow.endHour;
			
			nextFound = YES;
		}
		k++;
	}
	
	if (found) {
        
        //update the GUI back on the main thread
        dispatch_async(main_queue, ^{
            [self displayShow:currentShow andNext:nextShow];
        });
	}
	
	if (nextShow) [nextShow release];
	
	//[pool drain];
	
	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
	for(UITouch* touch in touches) {
		NSLog(@"UITouch = %@", touch);
		//NSLog(@"Going to open App Store at URL : %@", itmsURL);
		
		CGRect enveloppeFrame = ivEnveloppe.frame;
		CGRect labelFrame = lbSendMail.frame;
		CGPoint touchCoordinates = [touch locationInView:viewShowName];
		
		if (CGRectContainsPoint(enveloppeFrame, touchCoordinates) || CGRectContainsPoint(labelFrame, touchCoordinates)) {
			NSLog(@"Going to send an email");
			//if (playing)
				[self sendMailTo:currentShow.hostEmail withSubject:MAIL_SUBJECT];
			return;
		}
		
		CGRect itmsFrame = ivITMSButton.frame;
		touchCoordinates = [touch locationInView:itmsGroupView];
		if (CGRectContainsPoint(itmsFrame, touchCoordinates) && itmsURL) {
//		if (CGRectContainsPoint(itmsFrame, touchCoordinates) && ivITMSButton.alpha > 0.0f) {
			NSLog(@"Going to open App Store at URL : %@", itmsURL);
//			if (!itmsURL)
				[[UIApplication sharedApplication] openURL:[NSURL URLWithString:itmsURL]];

			//NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:itmsURL]] delegate:self startImmediately:YES];
			//[conn release];			
			
		}
	}
}	
/*
// Save the most recent URL in case multiple redirects occur
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    self.iTunesURL = [response URL];
    return request;
}

// No more redirects; use the last URL saved
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [[UIApplication sharedApplication] openURL:self.iTunesURL];
}
*/
#pragma mark iTunes Music Store

-(void)showITMSUI:(BOOL) show {
    [UIView animateWithDuration:0.5f delay: 0.0f
                        options:UIViewAnimationOptionCurveEaseInOut 
                     animations:^{
                         [ivITMSArtWork setAlpha:(show ? 1.0f : 0.0f)];
                         [ivITMSButton setAlpha:(show ? 1.0f : 0.0f)];
                     }
                     completion:^(BOOL finished) {							 
                     }
     ];
}

-(void)loadITMSURL:(id)title {
	
	if (! title ) {
		return;
	}
	
	//NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    dispatch_queue_t main_queue = dispatch_get_main_queue();
    
	NSAssert ([title isKindOfClass:[NSString class]], @"loadITMSURL : Object must be a NSString* !");
	
    //hack fro debugging TO BE REMOVED
	//title = @"Madonna - Into the Groove";
    
    //TODO : should I used the GTM version (one line) or the iOS SDK method (6 lines) or wrap the iOS SDK
    //       method in a NSString extension ????
    
	//remove parenthesis at the end of the title if any
	//NSString* title2 = [title gtm_stringByReplacingMatchesOfPattern:@"\\(.*\\).*$" withReplacement:@""];
    NSError* error = nil;
    NSString* title2;
    NSRegularExpression* regexp = [NSRegularExpression regularExpressionWithPattern:@"\\(.*\\).*$" options:NSRegularExpressionCaseInsensitive error:&error]; 
    
    if (!error) {
        title2 = [regexp stringByReplacingMatchesInString:title options:0 range:NSMakeRange(0, [title length]) withTemplate:@""];
    } else {
        NSLog(@"Can not apply regular expression replacement : %@", [error localizedDescription]);
        NSLog(@"Using orginal title instead (%@)", title);
        title2 = title;
    }
    
	NSLog(@"loadITMSURL : Title after Regex : %@", title2);
	
	//build the search URL
	NSString* encodedSongName = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)title2,NULL,(CFStringRef)@"!*’();:@&=+$,/?%#[]",kCFStringEncodingUTF8 );
    [encodedSongName autorelease];
    
    //do not search for France iTMS specifically
	//NSString* searchURL       = [NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStoreServices.woa/ws/wsSearch?term=%@&country=FR&media=music&entity=musicTrack&limit=1&genreId=&version=2&output=json", encodedSongName];
	NSString* searchURL       = [NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStoreServices.woa/ws/wsSearch?term=%@&media=music&entity=musicTrack&limit=1&genreId=&version=2&output=json", encodedSongName];
	NSURL*    url             = [NSURL URLWithString:searchURL];
	
	NSLog(@"loadITMSURL : encode string is = %@", searchURL);
	
	//get the results
	NSError* urlError = nil;
	NSString* jsonResult = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&urlError];
	
	if ( ! urlError && jsonResult) {
		
		//NSLog(@"loadITMSURL iTunes returned : %@", jsonResult);
		
		//parse them to retrieve the URL for iTMS and the ArtWork URL
		SBJSON* parser = [[[SBJSON alloc] init] autorelease];
		NSDictionary* results = [parser objectWithString:jsonResult];
		NSArray* allResult = (NSArray*)[results objectForKey:@"results"];
		
		if ([allResult count] > 0) {
			NSDictionary* entry = [allResult objectAtIndex:0];
			
			NSLog(@"loadITMSURL : NSDictionary is %@", entry);
		
			//prepare the applications buttons
			NSURL* imageURL = [NSURL URLWithString:[entry objectForKey:@"artworkUrl100"]];
			UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
			
			if (image) {
                
                //update the GUI back on the main thread
                dispatch_async(main_queue, ^{
                    [ivITMSArtWork setImage:image];
                    self.itmsURL = [entry objectForKey:@"trackViewUrl"];
                    
                    //show the buttons
                    [self showITMSUI:YES];
                });

			} else {
                //update the GUI back on the main thread
                dispatch_async(main_queue, ^{
                    //hide the buttons
                    [self showITMSUI:NO];
                
                    self.itmsURL = nil;
                });
			}
		
		} else {
            //no results to display
            
            //update the GUI back on the main thread
            dispatch_async(main_queue, ^{
                //hide the buttons
                [self showITMSUI:NO];
                self.itmsURL = nil;
            });
		}
		
		
	} else {
		NSLog(@"loadITMSURL : Can not find URLs : %@", [urlError description]);
		self.itmsURL = nil;
	}
	
	//[pool drain];
}

#pragma mark proximity sensor

- (void)sensorStateChange:(NSNotificationCenter *)notification
{
    if ([[UIDevice currentDevice] proximityState] == YES) {
        NSLog(@"Device is close to user - STOPPING.");
		[self changePlayStopStatus:NO];
    }
    else {
        NSLog(@"Device is ~not~ closer to user - STARTING.");
		[self changePlayStopStatus:YES];
    }
}

#pragma mark Initialization

-(void)loadStreamingURL {

	//NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    dispatch_queue_t main_queue = dispatch_get_main_queue();
	
	NSArray*  result = nil;
	NSError*  urlError;
	NSString* DELIMITER = @"\r\n";
	
	//
	// bug fix for Apple Rejection #2
	//
	NSURL* streamURLs;
	NSString* contentOfM3U;
	@try {
		
		///////
		// 
		// 1. Load settings
		//
		//////
		
		NSURL* settingsURLs = [NSURL URLWithString:MAXI80_APPS_SETTINGS];
		NSLog(@"Reading settings from : %@", [settingsURLs absoluteString]);
		self.settings = [NSDictionary dictionaryWithContentsOfURL:settingsURLs];
		
		//settings might be null when there is no network connection to the web site
		if (self.settings == nil) {
				NSString *pathToSettingsInBundle = [[NSBundle mainBundle] pathForResource:@"data_binary" ofType:@"plist"];
				NSLog(@"Reading settings from : %@", pathToSettingsInBundle);
				self.settings = [NSDictionary dictionaryWithContentsOfFile:pathToSettingsInBundle];
		}
		

		///////
		// 
		// 2. Load stream URLS
		//
		//////

		streamURLs = [NSURL URLWithString:[self.settings objectForKey:@"streamURL"]];
		contentOfM3U = [NSString stringWithContentsOfURL:streamURLs encoding:NSASCIIStringEncoding error:&urlError];

		//contentOfM3U is null when there is no network connection
		if (contentOfM3U == nil) {
			[self showAlertWithError:urlError];

            //run UIKit related code back on the main thread
            dispatch_async(main_queue, ^{
                [lbSongName songNameChange:NSLocalizedString(@"205", @"Pas de réseau - Réessayez plus tard") withAnimation:NO];
                btPlay.enabled = NO;
                btSend.enabled = NO;
                btDonate.enabled = NO;
                lbHelp.hidden = YES;
                [self hideAddBanner];
            });
            
		} else {
			
			NSArray* lines = [contentOfM3U componentsSeparatedByString:DELIMITER];
			NSRange range;
			range.location = 1; // to remove first line
			range.length = [lines count] - 3; //to remove first and last line
			result = [lines subarrayWithRange:range];
			streamingURLs = [result retain];
			
            //run UIKit related code back on the main thread
            dispatch_async(main_queue, ^{
                [btPlay setEnabled:YES];
                [btDonate setEnabled:YES];
            });
            
			NSLog(@"Array of URL = %@", result);
		}
		
		//this tests my safety net in changePlayStopStatus method
		//NSLog(@"TEST - PAUSE for 10s");
		//[NSThread sleepForTimeInterval:10]; 
        
	}
	@catch (NSException* ex) {

		NSLog(@"Exception when trying to connect to the stream : %@", ex);
		contentOfM3U= nil; //just to be sure
		urlError = nil;
	}
	//end of bug fix
	
	//[pool drain];
}

-(void)awakeFromNib {
}

- (void)viewDidLoad {
	
	NSLog(@"viewDidLoad");
    
    backgroundOpsQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	
	radio = nil;
	playing = NO;
	[self hideShowName];
	
	//add this object as receiver for the button (Play/Stop) events
	btPlay = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay target:self action:@selector(togglePlayStop:)];
	btPlay.style = UIBarButtonItemStyleBordered;
	btPause = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPause target:self action:@selector(togglePlayStop:)];
	btPause.style = UIBarButtonItemStyleBordered;
	
	NSMutableArray* items = [NSMutableArray arrayWithArray:toolbar.items];
	[items insertObject:btPlay atIndex:0];
	toolbar.items = items;
	
	//load the list of streaming URL
	[lbSongName loading];
	[btPlay setEnabled:NO];
	[btDonate setEnabled:NO];
    
    dispatch_async(backgroundOpsQueue, ^{
        [self loadStreamingURL]; 
    });
	//[self performSelectorInBackground:@selector(loadStreamingURL) withObject:nil];

	btVolume.target = self;
	btVolume.action = @selector(setSystemVolume:);
	
	btDonate.target = self;
	btDonate.action= @selector(showDonate:);
	
	btSend.target = self;
	btSend.action = @selector(sendMessage:);
	
	ivCover.delegate = self;
	hostImageCache = [[NSMutableDictionary alloc] init];
	

	//auto start playing one sec later
	[[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(delayedStartPlaying:) userInfo:nil repeats:NO] retain];
    
	iADBannerVisible = YES;
	[self hideAddBanner];
	wasAddBannerVisible = YES;
	
	[ivITMSArtWork setAlpha:0.0f];
	[ivITMSButton setAlpha:0.0f];

	volumeSliderVisible = NO;
    
	[super viewDidLoad];
}

#pragma mark help animation

-(void)showHelp:(BOOL)visible {
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0f];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:lbHelp cache:YES];
	[lbHelp setHidden:!visible];
	//lbHelp.layer.opacity = (visible ? 0.0f : 1.0f);
	[UIView commitAnimations];
	
	[ivCover showCover:!visible];
}

#pragma mark Real Controls

-(void)showHostPicture:(NSString*)pictureURL {
	
	//start the spinning network activity indicator
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	UIImage* image = [hostImageCache objectForKey:pictureURL];
	NSURL* url;
	
	// && pictureURL to solve a rare crash when there is no PictureURL provided
	if (!image && pictureURL) {
		url = [NSURL URLWithString:pictureURL];
		image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
		[hostImageCache setValue:image forKey:pictureURL];
	}
	
	//stop the spinning network activity indicator
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	if (image)
		NSLog(@"Picture Loaded Successfully");
	else 
		NSLog(@"Picture did not load : %@=%@", pictureURL, url); // will reschedule a load in n secs
	
	
	[ivCover setCoverImage:image withReflection:(image ? YES : NO)];
	coverShowing = NO;
}

-(void)showCover:(id)coverName {

	//NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    dispatch_queue_t main_queue = dispatch_get_main_queue();
    
	NSAssert ([coverName isKindOfClass:[NSString class]] || coverName == nil, @"showCover : Object must be nil or a NSString* !");
	
	
	NSString* string = [NSString stringWithFormat:@"%@/%@.jpg", [settings objectForKey:@"coverURL"], coverName];
	string = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSURL* url = [NSURL URLWithString:string];
	UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
	
	if (image)
		NSLog(@"Cover Loaded Successfully");
	else 
		NSLog(@"Cover dir not load : %@=%@", string, url); // will reschedule a load in n secs
	
    //run UIKit related code back on the main thread
    dispatch_async(main_queue, ^{

        [ivCover setCoverImage:image withReflection:(image ? YES : NO)];
        coverShowing = YES;
    });
	
	//[pool drain];
}

-(void)userDidTapCover:(CoverImageView*)coverImageView {
	
	if (coverShowing)
		[self showHostPicture:currentShow.url];
	else 
		[self showCover:lbSongName.text];
	
}

-(void)changePlayStopStatus:(BOOL)newStatus {
	
	//safety net if the list of URLs is not loaded yet
	NSRunLoop* loop = [NSRunLoop currentRunLoop];
	while (!streamingURLs) {
		//wait for the streamingURLs to be loaded
		NSLog(@"waiting streamingURLs to be loaded");
		
		//allow the run loop to fetch events
		[loop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1]];
	}

	//collect usage statistics
    //UA-11036224-2
    StatisticsCollector* statsCollect = [StatisticsCollector sharedStatisticsCollectorWithTrackingID:@"UA-11036224-2"];
    if (newStatus)
        [statsCollect recordStartEvent];
    else
        [statsCollect recordStopEvent]; 
	
	//avoid double clicking
	//if ([btPlay isEnabled]) {
		
		[btPlay setEnabled:NO]; //will not be effective before method's end


		if (newStatus) {
			
			//we gonna play, set the stop icon
			NSMutableArray* items = [NSMutableArray arrayWithArray:toolbar.items];
			[items replaceObjectAtIndex:0 withObject:btPause];
			toolbar.items = items;
			
			//hide help
			[self showHelp:NO];
			
			if (radio) {
				[radio updatePlay:YES];
			} else {
				radio = [[Radio alloc] init];
				//[radio connect:streamingURLs withDelegate:self withGain:([slVolume value])];
				[radio connect:streamingURLs withDelegate:self withGain:1.0f];
			}
			
			//register for incoming RemoteControl events
			NSLog(@"register to receive remote control event");
			[self becomeFirstResponder];
			[[UIApplication sharedApplication] beginReceivingRemoteControlEvents];		

		} else {
			
			//we gonna stop, set the play icon
			NSMutableArray* items = [NSMutableArray arrayWithArray:toolbar.items];
			[items replaceObjectAtIndex:0 withObject:btPlay];
			toolbar.items = items;
			 
			[radio updatePlay:NO];
			[lbSongName songNameChange:nil withAnimation:NO];
			
			//let the show name visible even when the sound is paused
			//[self hideShowName];
			
			//TODO : reactivate this when CoverImageView will have a mechanism to avoid changing cover hile animation is running
			//[self showCover:nil];

			//show help
			[self showHelp:YES];
			
			//hide the iTMS links
			[UIView animateWithDuration:0.5f delay: 0.0f
								options:UIViewAnimationOptionCurveEaseInOut 
							 animations:^{
								 [ivITMSArtWork setAlpha:0.0f];
								 [ivITMSButton setAlpha:0.0f];
							 }
							 completion:^(BOOL finished) {							 
							 }
			 ];			

		}
		
		playing = newStatus;

		[btPlay setEnabled:YES];
	//}	
}

//timer method to start playing after URLs loaded
- (void)delayedStartPlaying:(NSTimer*)theTimer {
	NSLog(@"Delayed Start Playing triggered");
	[theTimer release];
	[self changePlayStopStatus:YES];
}

#pragma mark iAD Management

- (void)showAddBanner {
	if (!iADBannerVisible) {
		[UIView animateWithDuration:1.0f delay:0.0f
							options:UIViewAnimationOptionCurveEaseInOut 
						 animations:^{
							 iAdsView.frame = CGRectOffset(iAdsView.frame, 0, -100);
						 }
						 completion:^(BOOL finished) {
							 iADBannerVisible = YES;
						 }
		 
		 ];
	}
}

-(void)hideAddBanner {
	
	if (iADBannerVisible) {
		[UIView animateWithDuration:1.0f delay:0.0f
							options:UIViewAnimationOptionCurveEaseInOut 
						 animations:^{
							 iAdsView.frame = CGRectOffset(iAdsView.frame, 0, 100);
						 }
						 completion:^(BOOL finished) {
							 iADBannerVisible = NO;
						 }
		 
		 ];
	}
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
	
	NSLog(@"bannerViewDidLoadAd");
	NSLog(@"iADBannerVisible = %d", iADBannerVisible);
	NSLog(@"volumeSliderVisible = %d", volumeSliderVisible);
	
	//if there is a banner change while the volume slider is visible, ensure the banner will be showed when the volume slider is hidden
	if (volumeSliderVisible)
		wasAddBannerVisible = YES;
	
    if (!iADBannerVisible && !volumeSliderVisible){
		
		NSLog(@"bannerViewDidLoadAd - banner was hidden, show it again");
		[self showAddBanner];
    }  
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
	
	NSLog(@"bannerView:didFailToReceiveAdWithError");
	if (iADBannerVisible) {
		
		NSLog(@"bannerView:didFailToReceiveAdWithError :  - banner was visible, hide it");
		[self hideAddBanner];
    }
}


#pragma mark UI Controls


-(IBAction)setSystemVolume:(id)sender {
	//should use MPVolumeView instead
	//MPVolumeSettingsAlertShow();
	
	if (!volumeSliderVisible) {
		NSLog(@"Showing System Volume");
		wasAddBannerVisible = iADBannerVisible;
		[self hideAddBanner];
		[UIView animateWithDuration:0.5f delay:(iADBannerVisible ? 1.0f : 0.0f)
						 options:UIViewAnimationOptionCurveEaseInOut 
						 animations:^{
							 [volumeSlider setAlpha:1.0f];
						 }
						 completion:^(BOOL finished) {
							 volumeSliderVisible = YES;
						 }
		 
		 ];
	} else {
		
		if (wasAddBannerVisible)
			[self showAddBanner];
		
		NSLog(@"Hiding System Volume");

		[UIView animateWithDuration:0.5f delay:0.0f
						 options:UIViewAnimationOptionCurveEaseInOut 
						 animations:^{
							 [volumeSlider setAlpha:0.0f];
						 }
						 completion:^(BOOL finished) {
							 volumeSliderVisible = NO;
						 }
		 
		 ];
	}
		
		
}

-(IBAction)togglePlayStop:(id)sender {
	//NSLog(@"togglePlayStop");
	@synchronized(self) {
		[self changePlayStopStatus:!playing];
	}
}

-(IBAction)showDonate:(id)sender {
	
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[settings objectForKey:@"donateURL"]]];    
    
    /*
	//suspend reflection and text animation
	[lbSongName stopAnimation];
	[ivCover showCover:NO];

	DonateViewController *webViewController = [[DonateViewController alloc] initWithPageURL:[settings objectForKey:@"donateURL"]];
	webViewController.delegate = self;
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
	
	[self presentModalViewController:navigationController animated:YES];
	
	[navigationController release];
	[webViewController release]; 
     */
}

/*
-(void) donateViewControlerDismissed:(DonateViewController*)controller {
	
	if (controller) {
		[self dismissModalViewControllerAnimated:YES];

		//reinit text animation and reflection
		[ivCover showCover:YES];
		[lbSongName resumeAnimation];
		
		//controller.view = nil;
		//[controller release];
	}
}
*/


#pragma mark RadioDelegate methods
//song name has changed
-(void)updateTitle:(NSString*)title {
	//NSLog(@"RadioDelegate : updateTitle : %@", title);
	
	[lbSongName songNameChange:title withAnimation:YES];
	
	//start a background thread to load and display the cover
	//[self performSelectorInBackground:@selector(showCover:) withObject:title];
    dispatch_async(backgroundOpsQueue, ^{
        [self showCover:title]; 
    });
    
	
	//start a background thread to find the link on iTunes Music Store
	//[self performSelectorInBackground:@selector(loadITMSURL:) withObject:title];
    dispatch_async(backgroundOpsQueue, ^{
        [self loadITMSURL:title]; 
    });

	//start background thread to find and display show name
	//[self performSelectorInBackground:@selector(findShowName) withObject:nil]; //will call displayShowName too
    dispatch_async(backgroundOpsQueue, ^{
        [self updateShowName]; 
    });
}

//gain was changed
-(void)updateGain:(float)value {
	//NSLog(@"RadioDelegate : updateGain");
}

//play status as updated
-(void)updatePlay:(BOOL)play {
	NSLog(@"RadioDelegate : updatePlay %d", play);
	[self changePlayStopStatus:play];
}

//buffering status has changed
-(void)updateBuffering: (BOOL)value {
	
	//sometime this method is called from a separate thread, without auto relase pool
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init] ;
	
	//NSLog(@"RadioDelegate : updateBuffering : %d", value);
	if ([radio isPlaying])
		lbSongName.buffering = value;
	
	[pool drain];
}

//can not reconnect 
-(void)connectionsFailed {
	[self showAlertWithError:nil];
	[self updatePlay:NO];
}

#pragma mark Orientation Changes

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	
	NSLog(@"shouldAutorotateToInterfaceOrientation");

    // Return YES for supported orientations
	return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {


	NSLog(@"willRotateToInterfaceOrientation");
	
	[lbSongName stopAnimation];
	//[self hideAddBanner];
	
	[super willRotateToInterfaceOrientation:interfaceOrientation duration:duration];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {

	CGRect frame1 = viewShowAndCover.frame;
	frame1.size.height = 130;
	CGRect frame2 = ivLogo.frame;
	CGRect frame3 = scrollView.frame;
	CGRect frame4 = volumeSlider.frame;
    CGRect frame5 = itmsGroupView.frame;

	//resize tsome component on the screen 
	if ((self.interfaceOrientation == UIInterfaceOrientationPortrait) || (self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)) {
		iAdsView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
		frame1.origin.y = 90;
		frame2.size.height = 60;
		frame3.origin.y = 290;
		frame4.origin.y = 383;
        frame5.origin.y = 226;
	} else {
		iAdsView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
		frame1.origin.y = 40;
		frame2.size.height = 40;
		frame3.origin.y = 180;
		frame4.origin.y = 230;
		frame4.size.height = 32;
        frame5.origin.y = 5;
	}
	
    //animate the resizing
	[UIView animateWithDuration:0.3f delay:0.0f
					options:UIViewAnimationOptionCurveEaseInOut 
					animations:^{
						viewShowAndCover.frame = frame1;
						ivLogo.frame = frame2;
						scrollView.frame = frame3;
						volumeSlider.frame = frame4;
                        itmsGroupView.frame = frame5;
					}
					 completion:^(BOOL finished) {}
	 
	 ];
	
	
	if ([ivCover isCoverAvailable])
		[ivCover drawReflection];

	//if (!volumeSliderVisible)
	//	[self showAddBanner];
	
	
	[lbSongName resumeAnimation];

}

#pragma mark Background management

-(void)notifyDidEnterBackground {
	NSLog(@"applicationDidEnterBackground");
	
	//stop song name animation
	[lbSongName stopAnimation];
}

-(void)notifyWillEnterForeground {
	NSLog(@"applicationWillEnterForeground");
	
	//resume songname animation
	[lbSongName resumeAnimation];
}

#pragma Mark Remote Control support

-(BOOL)canBecomeFirstResponder {
	NSLog(@"canBecomeFirstResponder : YES");
    return YES;
}


- (void)viewWillAppear:(BOOL)animated {
	NSLog(@"viewWillAppear");

    [super viewWillAppear:animated];
    [self becomeFirstResponder];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];

    //start notification about proximity sensor
    // Enabled monitoring of the sensor
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    
    // Set up an observer for proximity changes
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorStateChange:) 
                                                 name:@"UIDeviceProximityStateDidChangeNotification" object:nil];    
	
}


- (void)viewWillDisappear:(BOOL)animated {
	NSLog(@"viewWillDisappear");
    [super viewWillDisappear:animated];
    [self resignFirstResponder];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];

    //start notification about proximity sensor
    // Disabled monitoring of the sensor
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    
    // Remove an observer for proximity changes
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:@"UIDeviceProximityStateDidChangeNotification" object:nil];
	
}

- (void)remoteControlReceivedWithEvent:(UIEvent*)theEvent {
    
	NSLog(@"remoteControlReceivedWithEvent - %@", theEvent);
    if (theEvent.type == UIEventTypeRemoteControl) {
		NSLog(@"remoteControlReceivedWithEvent event is UIEventTypeRemoteControl, subtype is %d", theEvent.subtype);
		
        switch(theEvent.subtype) {
            case UIEventSubtypeRemoteControlPlay:
				NSLog(@"remoteControlReceivedWithEvent event is UIEventSubtypeRemoteControlPlay");
                [self changePlayStopStatus:!playing];
                break;
            case UIEventSubtypeRemoteControlPause:
				NSLog(@"remoteControlReceivedWithEvent event is UIEventSubtypeRemoteControlPause");
               [self changePlayStopStatus:!playing];
                break;
            case UIEventSubtypeRemoteControlStop:
				NSLog(@"remoteControlReceivedWithEvent event is UIEventSubtypeRemoteControlStop");
               [self changePlayStopStatus:!playing];
                break;
			case UIEventSubtypeRemoteControlTogglePlayPause:
				NSLog(@"remoteControlReceivedWithEvent event is UIEventSubtypeRemoteControlTogglePlayPause");
				[self changePlayStopStatus:!playing];
                break;
            default:
                return;
        }
    }
}

#pragma mark Unload and DeAlloc

-(void)alertViewCancel:(UIAlertView *)alertView
{
	[alert release];
	[self changePlayStopStatus:NO];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    NSLog(@"Maxi80ViewController - viewDidUnload");

	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    dispatch_release(backgroundOpsQueue);
}

- (void)dealloc {
	
    NSLog(@"Maxi80ViewController - dealloc");
	[hostImageCache release];
	self.settings = nil;
	self.btPlay = nil;
	self.btPause = nil;
	[streamingURLs release];
	[radio release];
	[super dealloc];
}

@end
