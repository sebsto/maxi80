//
//  SongNameLabel.h
//  Maxi80
//
//  Created by Sébastien Stormacq on 08/10/09.
//  Copyright 2009 Sébastien Stormacq. All rights reserved.
//
//
// This file is part of Maxi80 iPhone Application.
// 
// Maxi80 iPhone App is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2 of the License AND the 
// Common Development and Distribution License. 
// 
// Maxi80 iPhone App is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the Licenses
// along with Maxi80 iPhone App.  If not, see <http://www.gnu.org/licenses/>
// and http://www.sun.com/cddl/cddl.html.
//
//


#import <Foundation/Foundation.h>


//WARNING : this class assumes its superview is a UIScrollView
@interface SongNameLabel : UILabel {
	
	NSString* songName;
	BOOL      buffering;

}

@property (nonatomic, retain) NSString* songName;
@property (nonatomic) BOOL buffering;

-(void)songNameChange:(NSString*)newName withAnimation:(BOOL)animated;
-(void)loading;
-(void)stopAnimation;
-(void)resumeAnimation;

@end
