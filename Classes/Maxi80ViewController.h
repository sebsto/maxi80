//
//  Maxi80ViewController.h
//  Maxi80
//
//  Created by Sébastien Stormacq on 07/10/09.
//  Copyright 2009 Sébastien Stormacq. All rights reserved.
//
//
// This file is part of Maxi80 iPhone Application.
// 
// Maxi80 iPhone App is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2 of the License AND the 
// Common Development and Distribution License. 
// 
// Maxi80 iPhone App is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the Licenses
// along with Maxi80 iPhone App.  If not, see <http://www.gnu.org/licenses/>
// and http://www.sun.com/cddl/cddl.html.
//
//


#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MediaPlayer/MediaPlayer.h> //for volume control
#import <iAd/iAd.h>
#import "Radio.h"
#import "RadioDelegate.h"
#import "CoverImageView.h"
#import "SongNameLabel.h"
//#import "DonateViewController.h"

@interface Show : NSObject {
	NSString* name;
	NSString* hostName;
	NSString* hostEmail;
	NSString* url;
	NSInteger startHour;
	NSInteger endHour;
}

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* hostName;
@property (nonatomic, retain) NSString* hostEmail;
@property (nonatomic, retain) NSString* url;
@property (nonatomic) NSInteger startHour;
@property (nonatomic) NSInteger endHour;


@end

@interface Maxi80ViewController : UIViewController <RadioDelegate,
        //DonateViewControllerDelegate,
        UIActionSheetDelegate, CoverImageViewDelegate, 
        MFMailComposeViewControllerDelegate, ADBannerViewDelegate> {

	IBOutlet UIImageView*    ivLogo;
	IBOutlet ADBannerView*  iAdsView;
	IBOutlet UILabel* lbHelp;
	IBOutlet CoverImageView* ivCover;
	IBOutlet SongNameLabel*  lbSongName;
	IBOutlet UIScrollView*   scrollView;
	IBOutlet UIView*         viewShowAndCover;
	IBOutlet UIView*	 volumeSlider;
	
	//toolbar control
	IBOutlet UIToolbar* toolbar;
	UIBarButtonItem* btPlay;
	UIBarButtonItem* btPause;
	IBOutlet UIBarButtonItem* btVolume;
	IBOutlet UIBarButtonItem* btSend;
	IBOutlet UIBarButtonItem* btDonate;
	
	//show name control
	IBOutlet UIView*         viewShowName;
	IBOutlet UILabel*        lbCurrentShowName;
	IBOutlet UILabel*        lbCurrentShowTime;
	IBOutlet UILabel*        lbCurrentShowHost;
	IBOutlet UILabel*        lbNextShowTime;
	IBOutlet UILabel*        lbNextShowName;
	IBOutlet UILabel*        lbSendMail;
	IBOutlet UIImageView*    ivEnveloppe;
	
	//link to iTMS
	IBOutlet UIImageView*	 ivITMSArtWork;
	IBOutlet UIImageView*	 ivITMSButton;
    IBOutlet  UIView*        itmsGroupView;   
	
@private
	
	//toggle play/pause
	BOOL playing;
	
	//the streaming URLs
	NSArray* streamingURLs;
	
	//the player
	Radio* radio;
	
	//the applications settings and program guide
	NSDictionary* settings;
	
	//the alert when all connection attempts failed
	UIAlertView *alert;
	
	//the current show
	Show* currentShow;
	
	//flag to indicate wether we are showing the cover or the host / DJ photo
	BOOL coverShowing;
	
	//a cache for host image
	NSDictionary* hostImageCache;
	
	//is the iAD banner visible ?
	BOOL iADBannerVisible;
	BOOL wasAddBannerVisible;
	BOOL volumeSliderVisible;
	
	NSString* itmsURL;

    //background queue for async operations
    dispatch_queue_t backgroundOpsQueue;
}


@property (nonatomic, retain) IBOutlet ADBannerView*  iAdsView;
@property (nonatomic, retain) IBOutlet UILabel *lbHelp;
@property (nonatomic, retain) IBOutlet UIToolbar* toolbar;
@property (nonatomic, retain) UIBarButtonItem* btPlay;
@property (nonatomic, retain) UIBarButtonItem* btPause;
@property (nonatomic, retain) IBOutlet UIBarButtonItem* btVolume;
@property (nonatomic, retain) IBOutlet UIBarButtonItem* btSend;
@property (nonatomic, retain) IBOutlet UIBarButtonItem* btDonate;

@property (nonatomic, retain) IBOutlet UIImageView*    ivLogo;
@property (nonatomic, retain) IBOutlet UIView*	       volumeSlider;

//show name
@property (nonatomic, retain) IBOutlet UIView*		   viewShowName;
@property (nonatomic, retain) IBOutlet UIView*		   viewShowAndCover;
@property (nonatomic, retain) IBOutlet UILabel*        lbCurrentShowName;
@property (nonatomic, retain) IBOutlet UILabel*        lbCurrentShowTime;
@property (nonatomic, retain) IBOutlet UILabel*        lbCurrentShowHost;
@property (nonatomic, retain) IBOutlet UILabel*        lbNextShowTime;
@property (nonatomic, retain) IBOutlet UILabel*        lbNextShowName;
@property (nonatomic, retain) IBOutlet UILabel*        lbSendMail;
@property (nonatomic, retain) IBOutlet UIImageView*    ivEnveloppe;

//link to iTMS
@property (nonatomic, retain) IBOutlet UIImageView*		ivITMSButton;
@property (nonatomic, retain) IBOutlet UIImageView*     ivITMSArtWork;
@property (nonatomic, retain) NSString*     itmsURL;
@property (nonatomic, retain) UIView*       itmsGroupView;

@property (nonatomic, retain) IBOutlet SongNameLabel*  lbSongName;
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) NSDictionary* settings;
@property (nonatomic, retain) Show* currentShow;

-(void)notifyDidEnterBackground;
-(void)notifyWillEnterForeground;
-(void)hideAddBanner;

@end

