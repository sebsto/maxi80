//
//  Maxi80AppDelegate.m
//  Maxi80
//
//  Created by Sébastien Stormacq on 07/10/09.
//  Copyright 2009 Sébastien Stormacq. All rights reserved.
//
//
// This file is part of Maxi80 iPhone Application.
// 
// Maxi80 iPhone App is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2 of the License AND the 
// Common Development and Distribution License. 
// 
// Maxi80 iPhone App is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the Licenses
// along with Maxi80 iPhone App.  If not, see <http://www.gnu.org/licenses/>
// and http://www.sun.com/cddl/cddl.html.
//
//


#import "Maxi80AppDelegate.h"
#import "Maxi80ViewController.h"

@implementation Maxi80AppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
	//ask OS to continue while in background
	if ([UIDevice currentDevice].multitaskingSupported) {
		UIBackgroundTaskIdentifier backgroundTask = [application beginBackgroundTaskWithExpirationHandler:^{
			/* just fail if this happens. */
			NSLog(@"BackgroundTask Expiration Handler is called");
			[application endBackgroundTask:backgroundTask];
		}];
	}
	
	//made status bar black
	[application setStatusBarStyle: UIStatusBarStyleBlackOpaque];
	
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	[viewController notifyDidEnterBackground];
}

- (void) applicationWillEnterForeground:(UIApplication *)application {
	[viewController notifyWillEnterForeground];
}

- (void) applicationWillTerminate:(UIApplication *)application {
	NSLog(@"applicationWillTerminate");
}

- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
