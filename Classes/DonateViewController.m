//
//  DonateViewController.m
//  Maxi80
//
//  Created by Sébastien Stormacq on 13/10/09.
//  Copyright 2009 Sébastien Stormacq. All rights reserved.
//

#import "DonateViewController.h"

@implementation DonateViewController

@synthesize delegate;
@synthesize pageURL;

#pragma mark Error Reporting

-(void)showAlertWithError:(NSError*)error {
	
	NSString* title = NSLocalizedString(@"101", @"Impossible de se connecter");
	NSString* msgBase = NSLocalizedString(@"102", @"Nous ne pouvons pas nous connecter à Internet pour le moment. Vérifiez votre connection et assurez-vous d'être connecté à un réseau Edge/3G ou Wifi.");
	NSString* msg;
	if (error)
		msg = [NSString stringWithFormat:NSLocalizedString(@"104", @"%@\nCode d'erreur = %d"), msgBase, [error code]];
	else
		msg = msgBase;
	
	alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];			
}

-(void)alertViewCancel:(UIAlertView *)alertView
{
	[alert release];

}

#pragma mark Load and Initialize View

-(DonateViewController*)initWithPageURL:(NSString*)url {

	self = [super init];
	self.pageURL = url;
	
	return self;

}


-(void) loadView {
	
	//[super loadView];
		
	
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"103", @"Retour") style:UIBarButtonItemStyleDone target:self action:@selector(done:)] autorelease];
	self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStyleBordered;
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	
	NSURL* url;
	if (self.pageURL)
		url = [NSURL URLWithString:self.pageURL];
	else 
		url = [NSURL URLWithString:@"http://www.maxi80.com"];
		
	NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];

	UIWebView* webView = [[UIWebView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
								
	webView.scalesPageToFit = YES;
	webView.multipleTouchEnabled = YES;
	webView.delegate = self;
	
	[webView loadRequest:urlRequest];
	
	self.view = webView;

	[webView release];
}

-(void) done:(id)sender {

	UIWebView* webView = (UIWebView*)self.view;
	
	//stop loading if necessary
	if ([webView isLoading]) {
		[webView stopLoading];
	}
	
	//stop the spinning network activity indicator
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	//notify the application
	[self.delegate donateViewControlerDismissed:self];
	
}

#pragma mark orientation handler

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	
	NSLog(@"shouldAutorotateToInterfaceOrientation");
	
    // Return YES for supported orientations
	return YES;
}

#pragma mark WebView Delegate 

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {

	if (! [error code] == -999) //connection interupted by the user pressing the return button 
		[self showAlertWithError:error];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
	//NSLog(@"UIWebView didStartLoad");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
	//NSLog(@"UIWebView didFinishLoad");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
