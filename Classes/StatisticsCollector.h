//
//  StatisticsCollector.h
//  Maxi80
//
//  Created by Sebastien Stormacq on 24/08/10.
//  Copyright (c) 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface StatisticsCollector : NSObject {
@private
}

@property (retain) NSString*     trackingCode;


+(StatisticsCollector*)sharedStatisticsCollectorWithTrackingID:(NSString*)trackingID;

-(void)recordStartEvent;
-(void)recordStopEvent;

@end
