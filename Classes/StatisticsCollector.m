//
//  StatisticsCollector.m
//  Maxi80
//
//  Created by Sebastien Stormacq on 24/08/10.
//  Copyright (c) 2010 __MyCompanyName__. All rights reserved.
//

#import "StatisticsCollector.h"
#import "GANTracker.h"

@implementation StatisticsCollector

@synthesize trackingCode;


+(StatisticsCollector*)sharedStatisticsCollectorWithTrackingID:(NSString*)trackingID {
    
    static StatisticsCollector *sharedSingleton;
    
    @synchronized(self)
    {
        if (!sharedSingleton) {
            sharedSingleton = [[StatisticsCollector alloc] init];
        }
        
        sharedSingleton.trackingCode = trackingID;
        [sharedSingleton initGAN];
        return sharedSingleton;
    }
}    


-(void)recordStartEvent {

    NSError *error;    
    if (![[GANTracker sharedTracker] trackEvent:@"radio"
                                         action:@"start"
                                          label:@""
                                          value:-1
                                      withError:&error]) {
        NSLog(@"StatisticsCollector - ERROR while tracking START event : %@", [error description]);
    }    
}

-(void)recordStopEvent {
    
    NSError *error;    
    if (![[GANTracker sharedTracker] trackEvent:@"radio"
                                         action:@"stop"
                                          label:@""
                                          value:-1
                                      withError:&error]) {
        NSLog(@"StatisticsCollector - ERROR while tracking START event : %@", [error description]);
    }    
}

-(void)initGAN {
    UIDevice*     device = [UIDevice currentDevice];
    
    [[GANTracker sharedTracker] startTrackerWithAccountID:self.trackingCode
                                           dispatchPeriod:10
                                                 delegate:nil];
    
    [[GANTracker sharedTracker] setAnonymizeIp:NO];
    
    
    NSError *error;
    if (![[GANTracker sharedTracker] setCustomVariableAtIndex:1
                                                         name:@"deviceName"
                                                        value:[device name]
                                                    withError:&error]) {
        // Handle error here
        NSLog(@"StatisticsCollector - ERROR while setting custom variable 1 : %@", [error description]);
    }
    
    if (![[GANTracker sharedTracker] setCustomVariableAtIndex:2
                                                         name:@"deviceSystemName"
                                                        value:[device systemName]
                                                    withError:&error]) {
        // Handle error here
        NSLog(@"StatisticsCollector - ERROR while setting custom variable 2 : %@", [error description]);
    }
    
    if (![[GANTracker sharedTracker] setCustomVariableAtIndex:3
                                                         name:@"deviceSystemVersion"
                                                        value:[device systemVersion]
                                                    withError:&error]) {
        // Handle error here
        NSLog(@"StatisticsCollector - ERROR while setting custom variable 3 : %@", [error description]);
    }
    
    if (![[GANTracker sharedTracker] setCustomVariableAtIndex:4
                                                         name:@"deviceModel"
                                                        value:[device model]
                                                    withError:&error]) {
        // Handle error here
        NSLog(@"StatisticsCollector - ERROR while setting custom variable 4 : %@", [error description]);
    }
    
    /*
    if (![[GANTracker sharedTracker] trackPageview:@"/"
                                         withError:&error]) {
        // Handle error here
        NSLog(@"StatisticsCollector - ERROR when trackPagePreview : %@", [error description]);
    }    
    */
    
}

@end
