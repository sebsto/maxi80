//
//  CoverImageView.m
//  Maxi80
//
//  Created by Sébastien Stormacq on 08/10/09.
//  Copyright 2009 Sébastien Stormacq. All rights reserved.
//
//
// This file is part of Maxi80 iPhone Application.
// 
// Maxi80 iPhone App is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2 of the License AND the 
// Common Development and Distribution License. 
// 
// Maxi80 iPhone App is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the Licenses
// along with Maxi80 iPhone App.  If not, see <http://www.gnu.org/licenses/>
// and http://www.sun.com/cddl/cddl.html.
//
//


#import "CoverImageView.h"
#import "QuartzCore/QuartzCore.h" // for CALayer

#define COVER_APPEARING_ANIMATION_DURATION 2.0f
#define COVER_DISAPPEARING_ANIMATION_DURATION 0.5f
#define REFLECTION_ANIMATION_DURATION 0.5f

#define REFLECTION_ALPHA 0.4f

@implementation CoverImageView

@synthesize delegate;

#pragma mark Initialization

- (void)awakeFromNib {
	[super awakeFromNib];
	
	//allow to receive touches events
	self.userInteractionEnabled = YES;	
	reflection = nil;
}

#pragma mark Animation

-(void)releaseReflection {
	[reflection removeFromSuperview];
	[reflection release];
	reflection = nil;
}	


-(void) reflectionAppear {
	NSLog(@"REFLECTION_APPEARING");
	
	[UIView animateWithDuration:REFLECTION_ANIMATION_DURATION delay:0.0f
						options:UIViewAnimationOptionCurveEaseInOut 
					 animations:^{
						 //set the alpha to make reflection appear
						 reflection.layer.opacity = REFLECTION_ALPHA;
					 }
					 completion:^(BOOL finished) {
						 //nothing to do
					 }
	 ];

}

-(void) reflectionDisappear {
 	NSLog(@"REFLECTION_HIDDING");
	
	[UIView animateWithDuration:REFLECTION_ANIMATION_DURATION delay:0.0f
						options:UIViewAnimationOptionCurveEaseInOut 
					 animations:^{
						 //hide relection
						 reflection.layer.opacity = 0.0f;;
					 }
					 completion:^(BOOL finished) {
						 
						 if (finished) { 
							 
							 NSLog(@"Removing Reflection");
							 
							 //remove the reflection
							 [self releaseReflection];
						 }
					 }
	 ];

}


-(void)coverAppearing {
	NSLog(@"COVER_APPEARING");
	[self setImage:imgCover];
	
	
	/*
	[UIView animateWithDuration:COVER_APPEARING_ANIMATION_DURATION delay:0.0f
				options:UIViewAnimationOptionCurveEaseInOut
				animations:^{
					//show cover
					[self setHidden:NO];
				}
				completion:NULL
	 ];
	*/
	
	[UIView transitionWithView:self duration:COVER_APPEARING_ANIMATION_DURATION
					options:UIViewAnimationOptionTransitionFlipFromLeft
					animations:^{
						[self setHidden:NO];
					}
					completion:^(BOOL finished) {
						
						if (!noCover &&  willUseReflection && finished && !self.hidden) { 
							
							//set reflection if we have a real cover
							NSLog(@"Adding Reflection");
							[self drawReflection];
							
						}
					}
	];
	

}

-(void) coverDisappear {
	NSLog(@"COVER_HIDDING");
	
	[UIView animateWithDuration:COVER_DISAPPEARING_ANIMATION_DURATION delay:0.0f
					 options:UIViewAnimationOptionCurveEaseInOut
					 animations:^{
						 //hide cover
						 [self setHidden:YES];
					 }
					 completion:NULL
	 ];

	[UIView transitionWithView:self duration:COVER_DISAPPEARING_ANIMATION_DURATION
					options:UIViewAnimationOptionTransitionFlipFromLeft
					animations:^{}
					completion:^(BOOL finished) {
						
						if (finished) { 
							
							NSLog(@"Removing Cover");
							
							//destroy the previous image
							//[imgCover release]; 
							//imgCover = nil;
						}
					}
	 ];


}

#pragma mark custom control API

-(void)drawReflection {
	if (reflection)
		[self releaseReflection];
	
	reflection = [[ImageReflectionView imageReflectionViewWithImageView:self] retain];	
	reflection.layer.opacity = 0.0f;
	[self.superview addSubview:reflection];
	
	//animate reflection appearing
	[self reflectionAppear];
}


-(void)showCover:(BOOL)visible {

	NSLog(@"showCover : visible=%@ : noCover=%@ (imgCover = %@)", (visible ? @"YES" : @"NO"), (noCover ? @"YES" : @"NO"), imgCover);

	//on the device, sometime this method is called with a null image, causing non esthetical reflection
	if (!imgCover) {
		NSLog(@"Ignoring showCover");
		//return;
	}
	
	if (visible) {
	
		NSLog(@"showCover");
		
		//hide previous reflection if any
		if (reflection)
			[self reflectionDisappear];
		
		//show cover
		[self coverAppearing]; //wil trigger reflection appearing
		
		//show reflection if required (triggered by previous step)
		
	} else {
		
		//hide previous reflection if any
		if (reflection)
			[self reflectionDisappear];
		
		//hide cover
		[self coverDisappear];
	}

}

-(void)setCoverImage:(UIImage*)newImage withReflection:(BOOL)useReflection {
	
//TODO should implement a mechanism to avoid 
//     entering this function while an animation is in progress
	
	willUseReflection = useReflection;
	
	if (imgCover) {
		[imgCover release];
		imgCover = nil;
	}
	
	if (newImage) {
		imgCover = [newImage retain]; 
		noCover = NO;
	} else {
		imgCover = [[UIImage imageNamed:@"no-cover.png"] retain];
		noCover = YES;
	}
	
	[self showCover:YES];
}

-(BOOL)isCoverAvailable {
	return !noCover && !self.hidden;
}

#pragma mark HandleTouch

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	if (delegate)
		[delegate userDidTapCover:self];
}		


-(void)dealloc {
	
	if (reflection)
		[reflection release];
	
	if (imgCover)
		[imgCover release];
	
	[super dealloc];
}

@end
