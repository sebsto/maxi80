//
//  DonateViewController.h
//  Maxi80
//
//  Created by Sébastien Stormacq on 13/10/09.
//  Copyright 2009 Sébastien Stormacq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DonateViewControllerDelegate;

@interface DonateViewController : UIViewController <UIWebViewDelegate> {

	id <DonateViewControllerDelegate> delegate;
	NSString* pageURL;
	
	//the alert when connection fails
	UIAlertView *alert;
}


@property (nonatomic, retain) id<DonateViewControllerDelegate> delegate;
@property (nonatomic, retain) NSString* pageURL;

-(DonateViewController*)initWithPageURL:(NSString*)url;

@end


@protocol DonateViewControllerDelegate <NSObject>

//user dismissed the view
-(void) donateViewControlerDismissed:(DonateViewController*)controller;

@end