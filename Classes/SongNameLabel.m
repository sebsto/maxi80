//
//  SongNameLabel.m
//  Maxi80
//
//  Created by Sébastien Stormacq on 08/10/09.
//  Copyright 2009 Sébastien Stormacq. All rights reserved.
//
//
// This file is part of Maxi80 iPhone Application.
// 
// Maxi80 iPhone App is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2 of the License AND the 
// Common Development and Distribution License. 
// 
// Maxi80 iPhone App is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the Licenses
// along with Maxi80 iPhone App.  If not, see <http://www.gnu.org/licenses/>
// and http://www.sun.com/cddl/cddl.html.
//
//


#import "SongNameLabel.h"
#import "QuartzCore/QuartzCore.h" // for CALayer

#define INITIAL_LABEL @"www.maxi80.com"
#define BUFFERING_LABEL

@implementation SongNameLabel

@synthesize songName, buffering;

-(void)stopAnimation {
	[self.superview.layer removeAllAnimations];
}

-(void)resumeAnimation {
	[self songNameChange:self.songName withAnimation:YES];
}

-(void)loading {
	self.text = NSLocalizedString(@"001", @"Chargement...");
}

-(void)songNameChange:(NSString*)newName withAnimation:(BOOL)animated forBuffering:(BOOL)forBuffering {

	//NSLog(@"songNameChange : %@ : %d", newName, animated);
	
	if (!forBuffering)
		self.songName = newName; 

	//start the fade out
	[UIView animateWithDuration:0.5f delay:0.0f 
				options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
			    animations:^{
					 //real animation block
					 //change alpha value to make the text disapear
					 [self setAlpha:0.0f];
				}
				completion:^(BOOL finished) {
					 //animation is completed ?
					 if (finished) {
							 
						 self.text = (newName ? newName : INITIAL_LABEL);
						 
						 //stop previous animation
						 if (!animated) {
							 //stop previous animation (text scrolling)
							 [self.superview.layer removeAllAnimations];
						 }
						 
						 //ensure text is not truncated
						 [self sizeToFit];
						 ((UIScrollView*)self.superview).contentSize = self.frame.size;
						 
						 if (animated) {
							 
							 //start scrolling when needed
							 
							 //NSLog(@"SongNameLabel Animation : Text frame width        = %3.0f", self.frame.size.width);
							 //NSLog(@"SongNameLabel Animation : Scroll View frame width = %3.0f", self.superview.frame.size.width);
							 
							 //offset text by the width of the scroll view (to make it disapear on the right side)
							 ((UIScrollView*)self.superview).contentOffset = CGPointMake(0-self.superview.frame.size.width, 0.0f);
							 
							 [UIView animateWithDuration:10.0f delay:0.0f 
											  options:UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
											  animations:^{
												  //real animation block
												  //offset by the length of the text (to make it disapear on the left side)
												  ((UIScrollView*)self.superview).contentOffset = CGPointMake(self.frame.size.width, 0.0f);
											  }
											  completion:^(BOOL finished) {
											  }
							  ];
							 
						 } else {
							 
							 //center the content on the view
							 ((UIScrollView*)self.superview).contentOffset = CGPointMake(0-(self.superview.frame.size.width - self.frame.size.width) / 2, 0.0f);
						 }
						 
						 //start fade in
						 [UIView animateWithDuration:0.5f delay:0.0f 
										  options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
										  animations:^{
											  //real animation block
											  //set alpha to 1 to make the title appear
											  [self setAlpha:1.0f];
										  }
										  completion:^(BOOL finished) {
										  }
						  ];
					}
				}							 
	 ];
}

-(void)songNameChange:(NSString*)newName withAnimation:(BOOL)animated  {
	[self songNameChange:newName withAnimation:animated forBuffering:NO];
}

-(void)buffering:(BOOL)newBuffering {
	
	if (newBuffering) {
		
		buffering = YES;
		
		[self songNameChange:NSLocalizedString(@"002", @"Mise en mémoire tampon...") withAnimation:NO forBuffering:YES];
		
	} else {
		
		if (buffering) {
			NSLog(@"Animating SongName change");
			[self songNameChange:self.songName withAnimation:YES];
			buffering = NO;
		}
		
	}
}

-(void)dealloc {
	[super dealloc];
}


@end
